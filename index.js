var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var methodOverride = require('method-override')
var cors = require('cors');

var urlExists = require('url-exists');

var app = express();


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cors());


app.post('', function(req, res){
    urlExists(req.body.url , function(err, exists) {
        res.send({
            valid : exists
        })
    })
});


app.listen(process.env.PORT || 8080);

  